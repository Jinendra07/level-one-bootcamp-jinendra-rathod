#include<stdio.h>
typedef struct fraction
{
int num;
int deno;
}Fraction;
Fraction input()
{
	Fraction f;
	printf("\nEnter the numerator:");
	scanf("%d",&f.num);
	printf("Enter the denominator:");
	scanf("%d",&f.deno);
	return f;
}
int hcf(Fraction r)
{
    int i,gcd;
    for(i=1;i<=r.num && i<=r.deno;++i)
    {
        if(r.num%i==0 && r.deno%i==0)
            gcd=i;    
    }
    return gcd;
}
Fraction sum(Fraction f1,Fraction f2)
{
	int gcd;
	Fraction temp;
	Fraction r;
	temp.num=((f1.num*f2.deno)+(f2.num*f1.deno));
	temp.deno=(f1.deno*f2.deno);
	gcd=hcf(temp);
	r.num=temp.num/gcd;
	r.deno=temp.deno/gcd;
	return r;
}
void result(Fraction f1, Fraction f2, Fraction r)
{
    printf("The sum of %d/%d + %d/%d is %d/%d",f1.num,f1.deno,f2.num,f2.deno,r.num,r.deno);
}
int main()
{
	Fraction f1,f2,res;
	printf("Enter the numerator and denominator of the 1st fraction:");
	f1=input();
	printf("Enter the numerator and denominator of the 2nd fraction:");
	f2=input();
	res=sum(f1,f2);
	result(f1,f2,res);
	return 0;
}
