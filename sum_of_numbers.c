//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>
int input_n()
{
	int a;
	printf("Enter the number of elements in the array:");
	scanf("%d",&a);
    return a;
}
void input_array(int n,int a[100])
{
	for(int i=0;i<n;++i)
	{
		printf("Enter the element number %d of the array:",i+1);
		scanf("%d",&a[i]);
	}
}
int array_sum(int n,int a[100])
{
	int sum=0;
	for(int i=0;i<n;++i)
	{
		sum+=a[i];
	}
	return sum;
}
void result(int n,int a[100],int sum)
{
    int i;
	printf("The sum of ");
	for(i=0;i<n-1;++i){
		printf("%d+",a[i]);
	}
	printf("%d is %d",a[i],sum);
}
int main()
{
	int n,sum;
	n=input_n();
	int a[n];
	input_array(n,a);
	sum=array_sum(n,a);
	result(n,a,sum);
	return 0;
}
