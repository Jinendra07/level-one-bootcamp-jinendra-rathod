//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
int input()
{
int a;
scanf("%d",&a);
return a;
}
float distance_between_2_points(int x1,int y1,int x2,int y2)
{
float d;
d=sqrt(((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1)));
return d;
}
void result(float r)
{
printf("\nThe distance between 2 point is %.2f",r);
}
int main()
{
int x1,y1,x2,y2,dist;
printf("Enter the coordinates of point X:");
x1=input(x1);
y1=input(y1);
printf("Enter the coordinates of point Y:");
x2=input(x2);
y2=input(y2);
dist=distance_between_2_points(x1,y1,x2,y2);
result(dist);
return 0; 
}